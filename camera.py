import threading

class CameraThread(threading.Thread):
    '''
    Camera wrapper, uses threading
    '''
    def __init__(self, camera):
        threading.Thread.__init__(self)
        self.daemon = True
        self.camera = camera
        self.current_frame = None
        self.frame_lock = threading.Lock()

    def run(self):
        while(True):
            self.current_frame = self.camera.get_frame()

    def get_current_frame(self):
        frame = self.current_frame
        return frame
