##Systems Design Project 2015 - Group 5 - The Wizards of the Tower##

*Allan Brown, Sebastian Göb, Yordan Hristov, Boris Penev, Craig Snowden, Craig Wylie*

![alt text](https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/11023009_10206116260654371_687889155_n.jpg?oh=05f2b84261b31095c78926d10ab3cf4f&oe=553A968A&__gda__=1429960766_6169ca3d5892a2b4ecd6ec92b9d13ecb 'The wizard tower')
![alt text](https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpt1/v/t34.0-12/11130685_841752899249874_1302243278_n.jpg?oh=81dab5cbafd06787a061c210e83c6912&oe=553AA215&__gda__=1429905126_c323f32f44f7a02bfb62ead26f005471 'The wizards')
------


###Future SDP teams
The planner and vision are very good.

We used an Arduino powered robot. The Arduino specific stuff is in the communicatons repo.
If you change the communication on one end, you should change it on the robot itself too.
Keeping it in one repo is only natural.

The vision is very accurate with the correct calibration and achieves around 18 FPS.
Planner is based on a reactive system. We are the attackers so only implemented the attacker code.
The attacker strategy was the best in 2015. It depends on the build of the robot too.

Do not commit files other than source code.
This includes dynamically generated json files too.

###Running the system

Go to the root of the project.
Clone all the individual repositories.
Create a symbolic link to the controller launcher:
`ln -s controller/scripts/ctrl ./`
Then execute the newly created link.
`./ctrl <pitch_number> <our_side> <our_color>`
where *pitch_number* is either 0 for the main pitch and 1 for the secondary pitch.
Colors are yellow and blue. You can also use the y and b letters.
Side can be either left or right. You can also use the l and r letters.

####Vision controls
Press the following keys to switch the thresholding scheme:
*r* - red
*p* - plate
*b* - black dots

Note that the colors of the plates themselves are ignored - you do not need them.


------
###Installation

#### Linux/DICE

To install the needed software, execute the `opencv-download`, `opencv-build.sh`
and `opencv-config` scripts.

------
### Vision

* At the moment OpenCV + Python are being used. A [book](http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf) on Computer Vision with OpenCV in Python is a decent starting point about what OpenCV can do.
* A detailed tutorial with examples and use cases can be found [here](https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_tutorials.html) - going through it can be handy to understand the code

