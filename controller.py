from vision.vision import Vision, Camera, GUI
from planning.planner import Planner
from postprocessing.postprocessing import Postprocessing
from preprocessing.preprocessing import Preprocessing
from communications.communications import Communications, total_seconds
from communications.command import KickCommand, GrabCommand, StopCommand, \
        MoveCommand
from communications.mockcomm import MockComm
from communications.plancomm import PlannerSender, PlannerReceiver
from controller.camera import CameraThread
from planning.strategies import *
import vision.tools as tools
from cv2 import waitKey
import socket
import cv2
import serial
import warnings
import time
from datetime import datetime, timedelta


warnings.filterwarnings("ignore", category=DeprecationWarning)


class Controller:
    """
    Primary source of robot control. Ties vision and planning together.
    """

    def __init__(self, pitch, color, our_side, video_feed,
                 comm_port, delay, baud, timeout, comms,
                 local_ip, remote_ip,
                 lcl_snd_port, lcl_rcv_port, rmt_snd_port, rmt_rcv_port):
        """
        Entry point for the SDP system.

        Params:
            [int] video_feed                video feed for the camera
                                            if integer, id of the opened
                                            video capturing device
                                            if string, name of the opened
                                            video file
            [string] comm_port              port number for the arduino
            [int] pitch                     0 - main pitch, 1 - secondary pitch
            [string] our_side               the side we're on - 'left' or 'right'
            *[int] port                     The camera port to take the feed from
            *[Robot_Controller] attacker    Robot controller object - Attacker Robot has a RED
                                            power wire
            *[Robot_Controller] defender    Robot controller object - Defender Robot has a YELLOW
                                            power wire
        """
        assert pitch in [0, 1]
        assert color in ['yellow', 'blue']
        assert our_side in ['left', 'right']

        self.pitch = pitch
        self.color = color
        self.side = our_side

        # Set up camera
        self.camera = Camera(feed = video_feed, pitch = self.pitch)
        frame = self.camera.get_frame()
        frame_shape = frame.shape
        frame_center = self.camera.get_adjusted_center(frame)
        # WARNING needed to fix perspective distortion later
        # why frame is given as an argument ?!?
        # the center of the image is always the same (320,240) regardless
        # of the position of the pitch

        # Set up vision
        self.calibration = tools.get_colors(pitch)
        self.vision = Vision(pitch = self.pitch, color = self.color,
            our_side = self.side, frame_shape = frame_shape,
            frame_center = frame_center, calibration = self.calibration)

        # Set up preprocessing for vision
        self.preprocessing = Preprocessing()

        # Set up postprocessing for vision
        self.postprocessing = Postprocessing()

        # Set up communications
        self.communications = Communications(port = comm_port,
            baud = baud, timeout = timeout) if comms else MockComm(logcomm=False)

        # Set up planning communications
        self.sender   = PlannerSender(
                        host   = (local_ip,  lcl_snd_port),
                        client = (remote_ip, rmt_rcv_port))
        self.receiver = PlannerReceiver(
                        host   = (local_ip,  lcl_rcv_port),
                        server = (remote_ip, rmt_snd_port))

        # Set up main planner
        self.planner = Planner(our_side = self.side, pitch_num = self.pitch,
                communications = self.communications,
                sender = self.sender, receiver = self.receiver)

        # # Call the AttackerPass strategy for the milestone
        # self.attacker_pass = AttackerPass(self.planner._world,
        #         self.communications)

        # Set up GUI
        self.GUI = GUI(calibration = self.calibration,
                arduino = self.communications, pitch = self.pitch)

    def wow(self):
        """
        Ready your sword, here be dragons.
        """
        last_state = {}
        first_iteration = True
        last_timestamp = datetime.now()
        try:
            c = True
            while c != 27:  # the ESC key
                current_timestamp = datetime.now()
                timestep = total_seconds(current_timestamp - last_timestamp)
                # First iteration
                if first_iteration:
                    timestep = 0.1

                frame = self.camera.get_frame()
                if frame is None:
                    print 'no video frame, exiting'
                    break
                pre_options = self.preprocessing.options
                # Apply preprocessing methods toggled in the UI
                preprocessed = self.preprocessing.run(frame, pre_options)
                frame = preprocessed['frame']
                if 'background_sub' in preprocessed:
                    cv2.imshow('bg sub', preprocessed['background_sub'])

                # Find object positions
                # model_positions have their y coordinate inverted
                model_positions, regular_positions = self.vision.locate(frame)

                model_positions, current_state = self.postprocessing.analyze( \
                    model_positions, last_state, timestep, first_iteration)

                # Find appropriate action
                self.planner.update_world(model_positions, current_state)
                attacker_actions = None
                self.planner.plan()
                #defender_actions = self.planner.plan('defender')
                defender_actions = None

                # Information about the grabbers from the world
                grabbers = {
                    'our_defender': self.planner._world.our_defender.catcher_area,
                    'our_attacker': self.planner._world.our_attacker.catcher_area
                }

                # Information about states
                attackerState = (self.planner.attacker_state, self.planner.attacker_strat_state)
                defenderState = (None, None)

                # Use 'y', 'b', 'r' to change color.
                c = waitKey(2) & 0xFF
                actions = []
                fps = 1.0 / timestep

                # Draw vision content and actions
                self.GUI.draw(
                    frame, model_positions, actions, regular_positions, fps, attackerState,
                    defenderState, attacker_actions, defender_actions, grabbers,
                    our_color=self.color, our_side=self.side, key=c, preprocess=pre_options)
                last_state = current_state
                first_iteration = False
                last_timestamp = current_timestamp
        finally:
            self.vision.close_trackers()
            # Write the new calibrations to a file.
            tools.save_colors(self.pitch, self.calibration)
            self.communications.put_command(StopCommand())
            time.sleep(1)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("pitch",
                    choices=['p', '0', 's', '1'],
                    type=str.lower,
                    help='The pitch being used: [p, 0] = primary pitch'
                                              ' [s, 1] = secondary pitch',
                    metavar="pitch")
    parser.add_argument("side",
                    choices=['left', 'l', 'right', 'r'],
                    type=str.lower,
                    help="Our side of the pitch: [left, l, right, r] allowed.",
                    metavar="side")
    parser.add_argument("color",
                    choices=['blue', 'b', 'yellow', 'y'],
                    type=str.lower,
                    help="The color of our team: [blue, b, yellow, y] allowed.",
                    metavar="color")
    parser.add_argument('-p', '--port',
                    type = str,
                    dest = 'comm_port',
                    default = '/dev/ttyACM0',
                    required = False,
                    help = 'Port to send commands to the robot')
    parser.add_argument('-l', '--localip',
                        type = str,
                        dest = 'local_ip',
                        default = '127.0.0.1',
                        required = False,
                        help = 'IP address of our computer, ' \
                               'to communicate with our allies')
    parser.add_argument('-r', '--remoteip',
                        type = str,
                        dest = 'remote_ip',
                        default = '127.0.0.1',
                        required = False,
                        help = 'IP address of allied computer, ' \
                               'to communicate with us')
    parser.add_argument('--lsp', '--lsport',
                        type = int,
                        dest = 'lcl_snd_port',
                        default = 10724,
                        required = False,
                        help = 'Local sender port, ' \
                               'to send messages to our allies')
    parser.add_argument('--lrp', '--lrport',
                        type = int,
                        dest = 'lcl_rcv_port',
                        default = 28691,
                        required = False,
                        help = 'Local receiver port, ' \
                               'to receive messages from our allies')
    parser.add_argument('--rsp', '--rsport',
                        type = int,
                        dest = 'rmt_snd_port',
                        default = 50370,
                        required = False,
                        help = 'Remote sender port, ' \
                               'for our allies to send messages to us')
    parser.add_argument('--rrp', '--rrport',
                        type = int,
                        dest = 'rmt_rcv_port',
                        default = 47600,
                        required = False,
                        help = 'Remote receiver port, ' \
                               'for our allies to receive messages from us')
    parser.add_argument('--vp', '--vport',
                    type = int,
                    dest = 'video_port',
                    default = 0,
                    required = False,
                    help = 'Port to capture video')
    parser.add_argument('--vf', '--vfile',
                        type = str,
                        dest = 'video_file',
                        default = '',
                        required = False,
                        help = 'File to capture video')
    parser.add_argument('-d', '--delay',
                    type = float,
                    default = 0.5,
                    required = False,
                    help = 'Communication delay')
    parser.add_argument('-b', '--baud',
                    type = int,
                    default = 9600,
                    choices = [300, 600, 1200, 2400, 4800, 9600, 14400, 19200,
                               28800, 38400, 57600, 115200],
                    required = False,
                    help = 'Sets the data rate in bits per second (baud) for ' \
                           'serial data transmission. For communicating with ' \
                           'the computer, use one of these rates: 300, 600, ' \
                           '1200, 2400, 4800, 9600, 14400, 19200, 28800, ' \
                           '38400, 57600, or 115200.')
    parser.add_argument('-t', '--timeout',
                    type = float,
                    default = 0.1,
                    required = False,
                    help='Communication timeout')
    parser.add_argument('-n',
                    '--nocomms',
                    help='Disables sending commands to the robot',
                    action='store_true')

    args = parser.parse_args()

    pitch = {
        'p': 0,
        '0': 0,
        's': 1,
        '1': 1,
    }[args.pitch]

    side = {
        'left': 'left',
        'l': 'left',
        'right': 'right',
        'r': 'right',
    }[args.side]

    color = {
        'blue': 'blue',
        'b': 'blue',
        'yellow': 'yellow',
        'y': 'yellow',
    }[args.color]

    comm_port    = args.comm_port
    local_ip     = args.local_ip
    remote_ip    = args.remote_ip
    lcl_snd_port = args.lcl_snd_port
    lcl_rcv_port = args.lcl_rcv_port
    rmt_snd_port = args.rmt_snd_port
    rmt_rcv_port = args.rmt_rcv_port
    video_port   = args.video_port
    video_file   = args.video_file
    delay        = args.delay
    baud         = args.baud
    timeout      = args.timeout
    comms        = not args.nocomms

    if video_file != '':
        import os.path;
        if os.path.isfile(video_file):
            video_feed = video_file
        else:
            print 'File not found:', video_file
            return
        if video_port != 0:
            print 'Both non-default video port %d and video file %s ' \
                  'specified, the file is taking precedence.' % \
                   (video_port, video_file)
    else:
        import os.path;
        if os.path.exists('/dev/video' + str(video_port)):
            video_feed = video_port
        else:
            print 'Video device not found:', '/dev/video' + str(video_port)
            return

    print 'Pitch:\t\t\t' + ['PRIMARY', 'SECONDARY'][pitch]
    print 'Side:\t\t\t' + str.upper(side)
    print 'Color:\t\t\t' + str.upper(color)
    print 'Communication port:\t' + str(comm_port)
    print 'Local  IP address:\t' + str(local_ip)
    print 'Remote IP address:\t' + str(remote_ip)
    print 'Local  sender   port:\t' + str(lcl_snd_port)
    print 'Local  receiver port:\t' + str(lcl_rcv_port)
    print 'Remote sender   port:\t' + str(rmt_snd_port)
    print 'Remote receiver port:\t' + str(rmt_rcv_port)
    if video_file != '':
        print 'Video file:\t\t' + video_file
    else:
        print 'Video port:\t\t' + str(video_port)
    print 'Delay:\t\t\t' + str(delay)
    print 'Baud:\t\t\t' + str(baud)
    print 'Timeout:\t\t' + str(timeout)
    print 'Communications:\t\t' + str(comms)

    Controller(pitch = pitch, color = color, our_side = side,
        video_feed = video_feed, comm_port = comm_port, delay = delay,
        baud = baud, timeout = timeout, comms = comms,
        local_ip     = local_ip,     remote_ip    = remote_ip,
        lcl_snd_port = lcl_snd_port, lcl_rcv_port = lcl_rcv_port,
        rmt_snd_port = rmt_snd_port, rmt_rcv_port = rmt_rcv_port).wow()

if __name__ == '__main__':
    main()
