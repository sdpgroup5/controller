#!/bin/dash
# opencv-download.sh - A shell script to download the openCV source code
wget --output-document=opencv-2.4.10.zip \
	http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.10/opencv-2.4.10.zip
