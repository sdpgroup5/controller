#!/bin/dash
# Go to your sdp/ directory and do
# ln -s controller/scripts/status.sh ./
# To call the script do
# ./status.sh

set -o nounset
set -o errexit

for d in 'communications' 'controller' 'planning' 'postprocessing' \
         'preprocessing' 'vision'
do
  cd $d/
  echo $d/
  git status
  echo '\n'
  cd ../
done
