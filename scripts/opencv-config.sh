str='export PYTHONPATH=$PYTHONPATH:$HOME/lib/python2.6/site-packages:/usr/lib64/python2.6/site-packages'
grep -Fxq "$str" ~/.bash_profile || echo "$str" >> ~/.bash_profile
. ~/.bash_profile
virtualenv env
. env/bin/activate
pip install numpy polygon2 argparse pyserial

