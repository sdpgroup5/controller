#!/bin/dash
# Go to your sdp/ directory and do
# ln -s controller/scripts/reset.sh ./
# To call the script do
# ./reset.sh

set -o nounset
set -o errexit

for d in 'communications' 'controller' 'planning' 'postprocessing' \
         'preprocessing' 'vision'
do
  cd $d/
  echo $d/
  git clean --force
  git checkout master
  git fetch
  git reset --hard origin/master
  cd ../
done
