#!/bin/dash
# Go to your sdp/ directory and do
# ln -s controller/scripts/unpushed.sh ./
# To call the script do
# ./unpushed.sh
# branched.sh (previously it was unpushed.sh) shows the changes between
# origin/master and HEAD. However, if you are on a feature branch, it shows all
# the commits on it that are ahead of master. This script fixes this problem.
# It compares the current local branch to its remote tracking branch.
# You must set the remote tracking branch before running the script.
# For example:
# git branch --set-upstream master origin/master

set -o nounset
set -o errexit

for d in 'communications' 'controller' 'planning' 'postprocessing' \
         'preprocessing' 'vision'
do
  cd $d/
  echo $d/
  branch=`git for-each-ref --format='%(upstream)' $(git symbolic-ref -q HEAD)`
  git --no-pager log $branch..HEAD
  cd ../
done
