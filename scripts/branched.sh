#!/bin/dash
# Go to your sdp/ directory and do
# ln -s controller/scripts/branched.sh ./
# To call the script do
# ./branched.sh

set -o nounset
set -o errexit

for d in 'communications' 'controller' 'planning' 'postprocessing' \
         'preprocessing' 'vision'
do
  cd $d/
  echo $d/
  git --no-pager log origin/master..HEAD
  cd ../
done
