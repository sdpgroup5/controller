#!/bin/dash
# Go to your sdp/ directory and do
# ln -s controller/scripts/cleand.sh ./
# To call the script do
# ./cleand.sh

set -o nounset
set -o errexit

for d in 'communications' 'controller' 'planning' 'postprocessing' \
         'preprocessing' 'vision'
do
  cd $d/
  echo $d/
  git clean --dry-run
  cd ../
done
